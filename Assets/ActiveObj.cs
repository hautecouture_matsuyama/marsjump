﻿using UnityEngine;
using System.Collections;

public class ActiveObj : MonoBehaviour {

    [SerializeField]
    private GameObject aciveObject;

    public void Active()
    {
        aciveObject.SetActive(true);
    }
}
