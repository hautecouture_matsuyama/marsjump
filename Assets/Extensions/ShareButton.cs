﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ShareButton : MonoBehaviour {

    public static ShareButton Instance;

    [SerializeField]
    private Sprite shareSprite, closeSprite;

    [SerializeField]
    private GameObject backPanel;

    Sprite targetSprite;
	
			


    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start ()
    {
        twitterFrom = facebookFrom = LINEFrom = transform.localPosition;
        

	}

    public Transform twitterButton, facebookButton, LINEButton;

    bool moving;
    private Vector3 twitterFrom, facebookFrom, LINEFrom;
    public Vector3 twitterTo, facebookTo, LINETo;


    public void ShareCall()
    {

        backPanel.SetActive(!backPanel.activeSelf);
        
        if(targetSprite == closeSprite)
        {
            targetSprite = shareSprite;
        }

        else
        {
            targetSprite = closeSprite;
        }

        GetComponent<Image>().sprite = targetSprite;
        if (moving)  return;
        
            
        //StartCoroutine(ShareCallCor());
    }

    IEnumerator ShareCallCor()
    {
        moving = true;

        float timer = Time.time;
        float moveTime = 0.2f;
        Color color = new Color(1, 1, 1, 0);

        //移動済み.
        if (twitterButton.gameObject.activeInHierarchy)
        {
            while (Time.time - timer < moveTime)
            {
                float current = (Time.time - timer) / moveTime;

                //位置の移動.
                twitterButton.localPosition = twitterFrom * current + twitterTo * (1 - current);
                facebookButton.localPosition = facebookFrom * current + facebookTo * (1 - current);
                LINEButton.localPosition = LINEFrom * current + LINETo * (1 - current);

                //色を消す.
                color.a = (1 - current);
                twitterButton.GetComponent<Image>().color = color;
                facebookButton.GetComponent<Image>().color = color;
                LINEButton.GetComponent<Image>().color = color;
                yield return new WaitForEndOfFrame();
            }

            color.a = 0;
            twitterButton.GetComponent<Image>().color = color;
            facebookButton.GetComponent<Image>().color = color;
            LINEButton.GetComponent<Image>().color = color;

            //ボタンを消す.
            twitterButton.gameObject.SetActive(false);
            facebookButton.gameObject.SetActive(false);
            LINEButton.gameObject.SetActive(false);

            Color c = new Color();

            //メニューを有効にする.
            

            
        }
        //未移動.
        else
        {
            //ボタンの表示.
            twitterButton.gameObject.SetActive(true);
            facebookButton.gameObject.SetActive(true);
            LINEButton.gameObject.SetActive(true);

            Color c = new Color();

			

            while (Time.time - timer < moveTime)
            {
                float current = (Time.time - timer) / moveTime;

                //位置の移動.
                twitterButton.localPosition = twitterTo * current + twitterFrom * (1 - current);
                facebookButton.localPosition = facebookTo * current + facebookFrom * (1 - current);
                LINEButton.localPosition = LINETo * current + LINEFrom * (1 - current);

                //色を出す.
                color.a = current;
                twitterButton.GetComponent<Image>().color = color;
                facebookButton.GetComponent<Image>().color = color;
                LINEButton.GetComponent<Image>().color = color;
                yield return new WaitForEndOfFrame();
            }

        }



        color.a = 1;
        twitterButton.GetComponent<Image>().color = color;
        facebookButton.GetComponent<Image>().color = color;
        LINEButton.GetComponent<Image>().color = color;

        moving = false;
    }
    
}