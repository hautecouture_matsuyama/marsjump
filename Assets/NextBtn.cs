﻿using UnityEngine;
using System.Collections;

public class NextBtn : MonoBehaviour {

    [SerializeField]
    private GameObject nextObject;

    [SerializeField]
    private GameObject myObject;
	
    public void  Next()
    {
        myObject.SetActive(false);
        nextObject.SetActive(true);
    }
}
