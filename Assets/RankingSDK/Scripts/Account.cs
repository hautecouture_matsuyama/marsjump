﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Account : MonoBehaviour {

    [SerializeField]
    private GameObject CreateNameCanvas;

    void Awake()
    {
        QualitySettings.vSyncCount = 0; // VSyncをOFFにする
        Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定

        //ネット接続されているか
        if (Application.internetReachability == NetworkReachability.NotReachable)
            return;
        //アカウントは存在するか
        if (RankingManager.Instance.SignIn())
            return;

        StartCoroutine(ShowNameEdit());

    }

    private IEnumerator ShowNameEdit()
    {
        yield return new WaitForSeconds(0.5f);
        CreateNameCanvas.SetActive(true);

    }

}
