﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.SceneManagement;

public class RankingManager : SingletonMonoBehaviour<RankingManager>
{
    //今後新しいグループを追加したい場合は新規に記述
    private enum RANKING_GROUP
    {
        HC_GAMES,
        TUNER_JAPAN,
        OTHER,
    }

    //FirebaseのデータベースURL
    [SerializeField]
    private string _databaseUrl;

    [SerializeField]
    private RANKING_GROUP _rankingGroup;

    [SerializeField]
    private string _rankingChildItem;


    [SerializeField]
    private GameObject _content;
    [SerializeField]
    private GameObject _loadPanel;
    [SerializeField]
    private GameObject _createNameCanvas;
    [SerializeField]
    private GameObject _rankingCanvas;
    [SerializeField]
    private Button _editOKButton;
    [SerializeField]
    private Button _closeButton;
    [SerializeField]
    private Button _showRankingButton;

    [SerializeField]
    private Text _myrankText;

    [SerializeField]
    private InputField _inputfield;

    private DatabaseReference _rankingDb;
    private DatabaseReference _idListDb;

    private List<GameObject> _rankList = new List<GameObject>();
    private List<GameObject> _nameList = new List<GameObject>();
    private List<GameObject> scoreList = new List<GameObject>();
    private List<GameObject> IDList = new List<GameObject>();
    
    //後で消す
    private System.Random random = new System.Random();

    private int day;
    private int CourseDay = 0;//月曜からの経過日
    private DateTime lastDate;
    private TimeSpan daysNum;
    private DateTime date;


    void Awake()
    {
        if (this != Instance)
        {
            Destroy(this);
            return;
        }

        Initialize();

        Debug.Log("ユーザーIDは" + PlayerPrefs.GetString("userid"));
    }

    //サインアップ用関数
    public void SignUp(string name)
    {
        Guid userid = Guid.NewGuid();
        PlayerPrefs.SetString("userid", userid.ToString());
        PlayerPrefs.SetString("username", name);

        Dictionary<string, object> itemMap = new Dictionary<string, object>();
        
        itemMap.Add("name", name);
        itemMap.Add("score", 1);

        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add(userid.ToString() , itemMap);
        //データ更新！
        _rankingDb.UpdateChildrenAsync(map);

        _createNameCanvas.gameObject.SetActive(false);
    }


    //アカウント判定
    //既にユーザーIDがあれば、"true"が返ってくる
    public bool SignIn()
    {
        return PlayerPrefs.HasKey("userid");
    }

    //初期化用関数
    private void Initialize()
    {
		Debug.Log ("初期化するよ" + _databaseUrl);
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_databaseUrl);
		Debug.Log ("初期化sしたよ" + _databaseUrl);

        _rankingDb = FirebaseDatabase.DefaultInstance.GetReference(_rankingGroup.ToString()).Child(_rankingChildItem);

        //ランキングのノードをすべて取得
        foreach (var n in _content.gameObject.GetChildren())
        {
            //各リストに初期値挿入
            switch (n.name)
            {
                case "rText":
                    _rankList.Add(n);
                    break;
                case "nText":
                    _nameList.Add(n);
                    break;
                case "sText":
                    scoreList.Add(n);
                    break;
                case "uidText":
                    IDList.Add(n);
                    break;
            }

        }

        //初期値代入
        for (int i = 0; i < _rankList.Count; i++)
        {
            _rankList[i].GetComponent<Text>().text = i + 1 + "位";
            _nameList[i].GetComponent<Text>().text = "Player";
            scoreList[i].GetComponent<Text>().text = "" + 0;
        }

        //既に登録しているか？
        if (!SignIn())
        {
            _createNameCanvas.SetActive(true);
            _inputfield.text = SetDefaultName();
        }
            
        
        _editOKButton.onClick.AddListener( () => SignUp(_inputfield.text));
        _showRankingButton.onClick.AddListener(ShowRanking);
        _closeButton.onClick.AddListener(CloseButton);
    }

    //スコア送信関数
    public void SendScore(int score)
    {
        string userid = PlayerPrefs.GetString("userid");
        string name = PlayerPrefs.GetString("username");

        if (score > PlayerPrefs.GetInt("bestscore"))
        {
            Dictionary<string, object> itemMap = new Dictionary<string, object>();

            itemMap.Add("name", name);
            itemMap.Add("score", score);

            Dictionary<string, object> map = new Dictionary<string, object>();
            map.Add(userid, itemMap);

            _rankingDb.UpdateChildrenAsync(map);
        }
    }
    
    //ランキングの取得
    public void GetRankingData()
    {
        _loadPanel.SetActive(true);

        List<string> _scoreList = new List<string>();
        List<string> nameList = new List<string>();
        List<string> _idList = new List<string>();

        _rankingDb.OrderByChild("score").LimitToLast(100).GetValueAsync()
            .ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    //失敗した時
                    UnityEngine.Debug.Log("失敗した");
                }

                else if(task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    IEnumerator<DataSnapshot> en = snapshot.Children.GetEnumerator();

                    UnityEngine.Debug.Log("読み込むよ");

                    while (en.MoveNext())
                    {
                        DataSnapshot data = en.Current;
                        nameList.Add((string)data.Child("name").GetValue(true));
                        _scoreList.Add(data.Child("score").GetValue(true).ToString());
                        _idList.Add(data.Key);
                        UnityEngine.Debug.Log("読込中");
                    }

                    _loadPanel.SetActive(false);

                    //降順に並び替え
                    _scoreList.Reverse();
                    nameList.Reverse();
                    _idList.Reverse();

                    for (int i = 0; i < 100; i++)
                    {
                        scoreList[i].GetComponent<Text>().text = _scoreList[i];
                        _nameList[i].GetComponent<Text>().text = (int.Parse(_scoreList[i]) == 0) ? "Player" : _nameList[i].GetComponent<Text>().text = nameList[i];
                        IDList[i].GetComponent<Text>().text = (int.Parse(_scoreList[i]) == 0) ? null : _idList[i];

                        if (_idList[i] == PlayerPrefs.GetString("userid") && (int.Parse(_scoreList[i]) != 0))
                        {
                            scoreList[i].transform.parent.GetComponent<Image>().color = Color.yellow;
                        }
                        else
                        {
                            scoreList[i].transform.parent.GetComponent<Image>().color = new Color(0.42f, 0.2f, 0.075f, 0);
                        }
                        
                            _myrankText.text  = GetMyRankingData() + "位";
                        

                    }
                }

            });
    }

    public string GetMyRankingData()
    {
        string myScore = "";
        string myUserID = PlayerPrefs.GetString("userid");
        int scoreCnt =0;

        while(scoreCnt < 100)
        {
            if(IDList[scoreCnt].GetComponent<Text>().text == myUserID)
            {
                myScore = (scoreCnt + 1).ToString();

                return myScore;
            }

            scoreCnt++;
        }

        return "100位以下";
    }

    //デフォルトネームをセットする
    public string SetDefaultName()
    {
        string defaultName = "たろう";

        TextAsset nameTextFile = Resources.Load("Text/defaultname", typeof(TextAsset)) as TextAsset;
        string[] nameList = nameTextFile.text.Split("\n"[0]);
        int idx = random.Next(nameList.Length);
        defaultName = nameList[idx].Replace("\r", "");
        return defaultName;
    }

    //デバッグ用サインアップ
    public void DebugSignUp(string name)
    {
        Guid userid = Guid.NewGuid();
        PlayerPrefs.SetString("userid", userid.ToString());
        PlayerPrefs.SetString("username", name);

        Dictionary<string, object> itemMap = new Dictionary<string, object>();

        name = SetDefaultName();
        int score = random.Next(1000);

        itemMap.Add("name", name);
        itemMap.Add("score", score);

        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add(userid.ToString(), itemMap);
        //データ更新！
        _rankingDb.UpdateChildrenAsync(map);
    }

    private void CheackRankingPoint()
    {
        int num;
        daysNum = new TimeSpan(GetOneWeekCourse(), 0, 0, 0);//月曜日の日付を求めるためにTimeSpanを利用し月曜からの経過曜日数を取る
        date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出

        Debug.Log(date);

        if (PlayerPrefs.HasKey("OldDate"))
        {
            lastDate = System.DateTime.FromBinary(System.Convert.ToInt64(PlayerPrefs.GetString("OldDate")));
            //初期化日から何日たったか（現在の日数から前回の削除日（月曜日）を引く）
            CourseDay = System.DateTime.Now.Subtract(lastDate).Days;
            Debug.Log("経過日数" + CourseDay);
        }



        //経過日が月曜日を基準に7日を超えていたら（翌週月曜日）
        //初期化処理にて現在日付（今週月曜日）が保存されるので初期化をした週は下記処理を通らない
        if (CourseDay >= 7)
        {
            //初期化フラグを初期化
            PlayerPrefs.SetInt("DeleteFlag", 0);
        }


        //指定の変数が作られていない場合(初回起動時に一度だけの処理)
        if (!PlayerPrefs.HasKey("bestscore"))
        {
            PlayerPrefs.SetInt("bestscore", 0);
            PlayerPrefs.SetInt("OldMonth", System.DateTime.Now.Month);
            num = System.DateTime.Now.Day - GetOneWeekCourse();
            PlayerPrefs.SetInt("OldDay", num);
            date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出
            PlayerPrefs.SetString("OldDate", date.ToBinary().ToString());//月曜日の日付を保存
            PlayerPrefs.SetInt("DeleteFlag", 0);//0：false　　　1：true
        }


        //月曜日だったら
        if ("" + System.DateTime.Now.DayOfWeek == "Monday")
        {
            //初期化処理がされていない場合
            if (PlayerPrefs.GetInt("DeleteFlag") == 0)
            {
                //月曜日の段階で、現在の日付と前回の初期化日と比較し同じ日なら「初期化済み」違っていれば「初期化していない」とい判定に
                //また月を超えていた場合で月曜日なら初期化を行う（1日：1日だと同じ日として判定されてしまうため、月も判定に加える　4/1：5/1＝違う日）
                if ((System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day == PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month == PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")))
                {
                    //累計ポイント初期化
                    //各月、日付を現在日時に変更
                    PlayerPrefs.SetInt("bestscore", 0);
                    PlayerPrefs.SetInt("OldMonth", System.DateTime.Now.Month);
                    PlayerPrefs.SetInt("OldDay", System.DateTime.Now.Day);
                    date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出
                    PlayerPrefs.SetString("OldDate", date.ToBinary().ToString());//月曜日の日付を保存

                    //allAppleText.text = "0";//累計所持数を0の表示にする
                    /*ここに記入する*/

                    PlayerPrefs.SetInt("DeleteFlag", 1);
                }
            }
        }
        else//火曜～日曜の間なら
        {
            //削除処理がされていない場合
            if (PlayerPrefs.GetInt("DeleteFlag") == 0)
            {
                //経過日数が7日を超えた時（月曜日を基準に1週間を超えた場合）
                if (CourseDay > 7 && (System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day == PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month == PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")))
                {

                    //初期化処理
                    PlayerPrefs.SetInt("bestscore", 0);
                    PlayerPrefs.SetInt("OldMonth", System.DateTime.Now.Month);
                    num = System.DateTime.Now.Day - GetOneWeekCourse();
                    PlayerPrefs.SetInt("OldDay", num);
                    date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出
                    PlayerPrefs.SetString("OldDate", date.ToBinary().ToString());//月曜日の日付を保存
                                                                                 //allAppleText.text = "0";//累計所持数を0の表示にする
                                                                                 /*ここに記入する*/

                    PlayerPrefs.SetInt("DeleteFlag", 1);
                }
            }
        }
    }

    //各曜日ごとから月曜までの経過日数を取得
    private int GetOneWeekCourse()
    {

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Monday)
        {
            day = 0;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Tuesday)
        {
            day = 1;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
        {
            day = 2;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
        {
            day = 3;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Friday)
        {
            day = 4;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
        {
            day = 5;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
        {
            day = 6;
        }

        return day;
    }

   
    //ランキングの表示
    public void ShowRanking()
    {
        _rankingCanvas.SetActive(true);
        GetRankingData();
    }

    //名前登録関数
    private void CreateName()
    {
        _createNameCanvas.SetActive(true);
    }

    //名前変更
    private void ChangeName()
    {
        string userid = PlayerPrefs.GetString("userid");

        Dictionary<string, object> itemMap = new Dictionary<string, object>();

        itemMap.Add("name", _inputfield.text);
        itemMap.Add("score", PlayerPrefs.GetString("bestscore"));

        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add(userid, itemMap);

        _rankingDb.UpdateChildrenAsync(map);
    }

    private void CloseButton()
    {
        _rankingCanvas.SetActive(false);
    }

    private void GetMyrankingButton()
    {
        
    }

    public void DebugSignUp(int cnt, int wait)
    {
        StartCoroutine(SignUp(cnt, wait));

    }

    IEnumerator SignUp(int cnt, int wait)
    {
        int idx = 1;
        while (true)
        {
            int i = 0;

            while (i < cnt)
            {
                DebugSignUp("田中");

                i++;
            }
            _myrankText.text = idx + "回目";
            idx++;
            yield return new WaitForSeconds(wait);
        }
    }

    //private void DebugSendScore()
    //{
    //    SendScore(1000);
    //}

}

