﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class CheckCumulativeRankingPoint : MonoBehaviour {

    int day;
  //  public Text Mypoint;
    int CourseDay=0;//月曜からの経過日
    DateTime lastDate;
    TimeSpan daysNum;
    DateTime date;


    /*毎週月曜日の起動された時1度だけ初期化処理を行う
     * 日付から更新されたか、どれだけ経過したかをしらべる
     * 月曜意外で起動されかつ初期化が月曜に行われていない場合は初期化し今週月曜日の日付を取得しておく
     * 
    */

    void Start()
    {
        CheackRankingPoint();
    }

    public void CheackRankingPoint()
    {
            int num;
            daysNum = new TimeSpan(GetOneWeekCourse(), 0, 0, 0);//月曜日の日付を求めるためにTimeSpanを利用し月曜からの経過曜日数を取る
            date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出

            Debug.Log(date);

            if (PlayerPrefs.HasKey("OldDate"))
            {
                lastDate = System.DateTime.FromBinary(System.Convert.ToInt64(PlayerPrefs.GetString("OldDate")));
                //初期化日から何日たったか（現在の日数から前回の削除日（月曜日）を引く）
                CourseDay = System.DateTime.Now.Subtract(lastDate).Days;
                Debug.Log("経過日数" + CourseDay);
            }



            //経過日が月曜日を基準に7日を超えていたら（翌週月曜日）
            //初期化処理にて現在日付（今週月曜日）が保存されるので初期化をした週は下記処理を通らない
            if (CourseDay >= 7)
            {
                //初期化フラグを初期化
                PlayerPrefs.SetInt("DeleteFlag", 0);
            }


            //指定の変数が作られていない場合(初回起動時に一度だけの処理)
            if (!PlayerPrefs.HasKey("bestscore"))
            {
                PlayerPrefs.SetInt("bestscore", 0);
                //  Mypoint.text = PlayerPrefs.GetInt("Cumulative").ToString();
                PlayerPrefs.SetInt("OldMonth", System.DateTime.Now.Month);
                num = System.DateTime.Now.Day - GetOneWeekCourse();
                PlayerPrefs.SetInt("OldDay", num);
                date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出
                PlayerPrefs.SetString("OldDate", date.ToBinary().ToString());//月曜日の日付を保存
                PlayerPrefs.SetInt("DeleteFlag", 0);//0：false　　　1：true
            }


            //月曜日だったら
            if ("" + System.DateTime.Now.DayOfWeek == "Monday")
            {
                //初期化処理がされていない場合
                if (PlayerPrefs.GetInt("DeleteFlag") == 0)
                {
                    //月曜日の段階で、現在の日付と前回の初期化日と比較し同じ日なら「初期化済み」違っていれば「初期化していない」とい判定に
                    //また月を超えていた場合で月曜日なら初期化を行う（1日：1日だと同じ日として判定されてしまうため、月も判定に加える　4/1：5/1＝違う日）
                    if ((System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day == PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month == PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")))
                    {
                        //累計ポイント初期化
                        //各月、日付を現在日時に変更
                        PlayerPrefs.SetInt("bestscore", 0);
                        PlayerPrefs.SetInt("OldMonth", System.DateTime.Now.Month);
                        PlayerPrefs.SetInt("OldDay", System.DateTime.Now.Day);
                        date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出
                        PlayerPrefs.SetString("OldDate", date.ToBinary().ToString());//月曜日の日付を保存
                        PlayerPrefs.SetInt("DeleteFlag", 1);
                    }
                }
            }
            else//火曜～日曜の間なら
            {
                //削除処理がされていない場合
                if (PlayerPrefs.GetInt("DeleteFlag") == 0)
                {
                    //経過日数が7日を超えた時（月曜日を基準に1週間を超えた場合）
                    if (CourseDay > 7 && (System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day == PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month != PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")) || (System.DateTime.Now.Month == PlayerPrefs.GetInt("OldMonth") && System.DateTime.Now.Day != PlayerPrefs.GetInt("OldDay")))
                    {

                        //初期化処理
                        PlayerPrefs.SetInt("bestscore", 0);
                        PlayerPrefs.SetInt("OldMonth", System.DateTime.Now.Month);
                        num = System.DateTime.Now.Day - GetOneWeekCourse();
                        PlayerPrefs.SetInt("OldDay", num);
                        date = System.DateTime.Now - daysNum;//現在日付から曜日経過日数を引いて月曜日の日付を算出
                        PlayerPrefs.SetString("OldDate", date.ToBinary().ToString());//月曜日の日付を保存
                        PlayerPrefs.SetInt("DeleteFlag", 1);
                    }
                }
            }
    }


    //各曜日ごとから月曜までの経過日数を取得
    int GetOneWeekCourse()
    {
        
        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Monday)
        {
            day = 0;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Tuesday)
        {
            day = 1;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
        {
            day = 2;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
        {
            day = 3;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Friday)
        {
            day = 4;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
        {
            day = 5;
        }

        if (System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
        {
            day = 6;
        }

        return day;
    }
}
