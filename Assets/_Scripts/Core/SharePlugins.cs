﻿using UnityEngine;
using System.Collections;

public class SharePlugins : MonoBehaviour
{
    public Game game;

    public void OnClick()
    {
        if(gameObject.name=="share_facebook")
        {
            game.ShareFacebook();
        }

        if (gameObject.name == "share_line")
        {
            game.ShareLINE();
        }

        if (gameObject.name == "share_twitter")
        {
            game.ShareTwitter();
        }
    }

}
