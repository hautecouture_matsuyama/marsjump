﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

[System.Serializable]
public struct UpgradeItem
{
	public Text info;
	public Image levelIndicator;
	public Image iconImage;

	public Sprite[] UpgradeIcons;
	public Sprite[] UpgradeIcons_zh;
}

[System.Serializable]
public struct UpgradeVisuals
{
	public Sprite[] levelIndicators;
	public UpgradeItem[] items;
}

[System.Serializable]
public struct VisualSettings 
{
	public float[] planetRadius;
	public Sprite[] planetSprites;
	public Sprite[] planetSprites_zh;
	public GameObject[] atoms;
	public GameObject[] FireBall;
	public GameObject ShootingStar;

	[Space (5)]
	public UpgradeVisuals upgradeVisuals;

	[Space (5)]
	public Text MetersText;
	public Text AtomsText; 

	[Space (10)]
	public AudioClip[] atomSounds;
	public AudioClip JumpSound;
}

public class Bank
{
	private int BankBalance = 0;

	public Bank ()
	{
		// Load Data
		this.BankBalance = PlayerPrefs.GetInt ("_BankBalance", 0);    
	}

	public void Deposit (int amount)
	{
		this.BankBalance += amount;

		PlayerPrefs.SetInt ("_BankBalance", this.BankBalance);
	}

	public void Withdraw (int amount)
	{
		this.BankBalance -= amount;

		PlayerPrefs.SetInt ("_BankBalance", this.BankBalance);
	}

	public int GetBalance ()
	{
		return this.BankBalance;
	}
}

public class Game : MonoBehaviour 
{

    public static bool gameOverFlg = false;



    //現在の順位
    [SerializeField]
    private Text myRank;



	//private AdmobScript admobscript;

	// -- Singleton 
	// -- Singleton
	private static Game _instance;
	public static Game instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType <Game> ();
			}
			
			return _instance;
		}
	}

	private Player player;
	private PlanetManager planetManager;

	public VisualSettings visuals;

	private Planet currentPlanet;

	public bool paused { get; set; }

	// -- Current number of atoms collected
	private int AtomCount = 0;

	// -- Stores our meters travelled. 
	private int _metersTraveled;
	public int metersTraveled
	{
		get { return _metersTraveled; }
		
		set
		{
			if (value > _metersTraveled)
			{
				// -- Increased
				_metersTraveled = value;
			}
		}
	}
	
	// -- forced starting planet
	private Vector3 StartPosition {
		get {
			return new Vector3 (0, 3.5f, -0.001f);
		}
	}

	// -- UI CRAP

	public Vector2 AtomsTweenTarget
	{
		get
		{
			return Camera.main.ScreenToWorldPoint (GameObject.Find ("Atoms_Counter_Img").transform.position);
		}
	}

	public bool _IsRunning { get; set; }
	private bool tmpDead = false;

	private AchievementsManager _AchievementManager;
	private List <Achievement> _ActiveAchievements = new List <Achievement> (3);

	public Bank _Bank { get; set; }

	public const int ads = 0; // 0 = on, 1 = off

    string storeUrl = "";

	public void Awake ()
	{

		this._AchievementManager = new AchievementsManager ();
		this._AchievementManager.LoadAchievements ();

		this.player = GameObject.Find ("_Cat_Go").GetComponent <Player> ();

		// -- Load Bank
		this._Bank = new Bank ();

		// -- Initialise tween engine
		DOTween.Init (true, true, LogBehaviour.ErrorsOnly);

		Application.targetFrameRate = 60;

		// -- are ads on ? 
		if (ads == 0)
		{
			#if UNITY_IPHONE || UNITY_ANDROID
			// Replace with your app IDs and app signatures ... 
			//Chartboost.init ("54c6cc2943150f17afbbff1b", "8ecbfd2fd8eaab405ef8e08f4c38f1e89e6618a8", "54c6cb2143150f178d293089", "df3bea981e891069e0fa34eed07ec8d9bc804eb9");
			//admobscript = new AdmobScript();
			#endif
		}

        

#if UNITY_ANDROID
        storeUrl = "https://play.google.com/store/apps/details?id=com.hautecouture.marsbaby";
#elif UNITY_IPHONE
        storeUrl = "https://play.google.com/store/apps/details?id=com.hautecouture.ppg";
#endif

    }

    public void Start ()
	{
		this._AchievementManager.AchievementUnlocked += (sender, e) =>
		{
			UnlockAchievement (e.Data);
		};

		this.AtomCount = 0;
		this.metersTraveled = 0;
	}

	private int _AtomMultiplier = 1;

    public void SwitchFlg()
    {
        gameOverFlg = !gameOverFlg;
    }


	public void StartGame () 
	{
		// -- Create out planet manager, handles creating the planets.
		planetManager = new PlanetManager (this.visuals.planetSprites, this.visuals.planetRadius, this.visuals.atoms);

		///planetManager.CleanUp ();

		this.player.GetComponent <Rigidbody2D> ().gravityScale = 1.0f;
		this.player.transform.position = this.StartPosition;
		this.player.Init ();

		this._metersTraveled = 0;
		this.visuals.MetersText.text = this.metersTraveled.ToString ("D4") + "s";

		this.AtomCount = 0;
		this.visuals.AtomsText.text = this.AtomCount.ToString ();
		
		this._IsRunning = true;
		this.paused = false;
		this.tmpDead = false;

		planetManager.Init (this.player);

		if (_AchievementManager != null)
			_AchievementManager.RegisterEvent (AchievementType.GameCount, this._ActiveAchievements.ToArray (), 1);

		this._AtomMultiplier = PlayerPrefs.GetInt ("_AtomMultiplier", 0);

		Camera.main.transform.position = new Vector3 (0, 0, -10);

		#if UNITY_IPHONE || UNITY_ANDROID
		//Chartboost.cacheInterstitial ();
		//admobscript = new AdmobScript();
		#endif
	}

	void Update () 
	{
		if (planetManager == null)
			return;

		if (IsDead ())
		{
			if (!this.tmpDead)
			{
				Die ();

				this.tmpDead = true;
			}

			return;
		}

		planetManager.Update ();

		// -- only update if we're not travelling backwards
		this.metersTraveled = (Mathf.RoundToInt (StartPosition.x + player.transform.position.x));

		// -- update text
		string formattedScore = this.metersTraveled.ToString ("D4");
		this.visuals.MetersText.text = formattedScore + " m";

		DoCameraEveryFrame ();
	}

	public void OnPlanetLand ()
	{
		if (planetManager != null)
		{
			SetCurrentPlanet ();

            //生成
			planetManager.NewPlanet ();

			//UpdateCamera ();
		}
	}

	public void Die (bool SwitchScreen = true)
	{
		this._IsRunning = false;

		this.player.transform.SetParent (null);
		this.player.GetComponent <Rigidbody2D> ().gravityScale = 0.0f;
		this.player.GetComponent <Rigidbody2D> ().velocity = Vector2.zero;
		this.player.transform.position = new Vector3 (1000, 1000, 0);

		if (planetManager != null)
			planetManager.CleanUp ();
		
		this.planetManager = null;

		if (SwitchScreen) {
            if (InterfaceManager.instance.currentScreen != ActiveScreen.Death)

            BackButtonUtil.Instance.ChangeCommand(BackKeyAction.InvokeEvent);
            BackButtonUtil.Instance.RemoveListenerAll();
            BackButtonUtil.Instance.AddListener(InterfaceManager.instance.SelectMenu);
            InterfaceManager.instance.SwitchScreen(ActiveScreen.Death);
        }
		else
			return;

		// --
		if (_AchievementManager != null)
			_AchievementManager.RegisterEvent (AchievementType.DeathCount, this._ActiveAchievements.ToArray (), 1);

		_Bank.Deposit (this.AtomCount);

		StoreBestMeters (this.metersTraveled);
		StoreBestAtoms (this.AtomCount);

		DisplayGameStats ();
	}

    static int interstitialCnt = 0;

	public void DisplayGameStats ()
	{
		Text textObj;
		string textBuffer;
		string basetext;
		// -- BEST METERS
		int _BestMeters = PlayerPrefs.GetInt ("_BestMeters"); 
		textObj = GameObject.Find ("Best_Meters_Text").GetComponent <Text> ();
		textBuffer = textObj.text;
		basetext = textBuffer.Substring(0,textBuffer.IndexOf(":")+1);
		textObj.text = basetext + " " + _BestMeters.ToString ("N0") + "m";

		// -- BEST ATOMS
		int _BestAtoms = PlayerPrefs.GetInt ("_BestAtoms"); 
		textObj = GameObject.Find ("Best_Atoms_Text").GetComponent <Text> ();
		textBuffer = textObj.text;
		basetext = textBuffer.Substring(0,textBuffer.IndexOf(":")+1);
		textObj.text = basetext + " " + _BestAtoms.ToString ("N0");
		
		// -- Current Meters
        GameObject.Find("Final_Meters_Text").GetComponent<Text>().text = this.metersTraveled.ToString("N0") + "m";

        Debug.Log("GAMEOVER");

        gameOverFlg = true;

        if(this.metersTraveled > PlayerPrefs.GetInt("bestscore"))
        {
            RankingManager.Instance.SendScore(this.metersTraveled);
            PlayerPrefs.SetInt("bestscore", this.metersTraveled);
        }

        RankingManager.Instance.GetRankingData();


        //StartCoroutine(ShowInterStitial());

        NendUnityPlugin.AD.NendAdInterstitial.Instance.Show();
        
        
		// -- Current Atoms
		GameObject.Find ("Final_Atoms_Text").GetComponent <Text> ().text = this.AtomCount.ToString ("N0");

		// -- Bank
		textObj = GameObject.Find ("Bank_Text").GetComponent <Text> ();
		textBuffer = textObj.text;
		basetext = textBuffer.Substring(0,textBuffer.IndexOf(":")+1);
		textObj.text = basetext + " " + this._Bank.GetBalance ().ToString ("N0");
	}

    private IEnumerator ShowInterStitial()
    {
        float time = Random.Range(0.2f, 0.5f);
        yield return new WaitForSeconds(time);
        NendUnityPlugin.AD.NendAdInterstitial.Instance.Show();

    }

    
	void StoreBestMeters (int newMeters)
	{
		int oldMeters = PlayerPrefs.GetInt ("_BestMeters", 0);    

		if (newMeters > oldMeters)
		{
			PlayerPrefs.SetInt ("_BestMeters", newMeters);
		}
	}

	void StoreBestAtoms (int newAtoms)
	{
		int oldAtoms = PlayerPrefs.GetInt ("_BestAtoms", 0);    
		
		if (newAtoms > oldAtoms)
		{
			PlayerPrefs.SetInt ("_BestAtoms", newAtoms);
		}
	}

	/// <summary>
	/// Tweens the atom
	/// </summary>
	public void ProcessAtom (Atom _atom)
	{
		if (!_atom.collected)
		{
			AtomCount += (int)(_atom.value * UpgradeInfo.GetMultiplierPower (this._AtomMultiplier));

			if (_AchievementManager != null)
				_AchievementManager.RegisterEvent (AchievementType.CollectAtoms, this._ActiveAchievements.ToArray ());

			if (_atom.GetAtomType () == AtomType.Special)
			{
				if (_AchievementManager != null)
					_AchievementManager.RegisterEvent (AchievementType.CollectSpecialAtoms, this._ActiveAchievements.ToArray ());
			}

			// -- Set Text
			//string formattedAtomCount = this.AtomCount.ToString ("D2");
			this.visuals.AtomsText.text = this.AtomCount.ToString ();

			GetComponent<AudioSource>().PlayOneShot (this.visuals.atomSounds [Random.Range (0, 3)]);

			_atom.collected = true;
		}
	}

	void UnlockAchievement (Achievement data)
	{
		for (int i = 0; i < this._ActiveAchievements.Count; i++)
		{
			if (data.Message == this._ActiveAchievements[i].Message)
			{
				Debug.Log ("Unlocked: " + data.Message);
				
				RectTransform AchievmentRect = GameObject.Find ("Achievement_Image").GetComponent <RectTransform> ();
				
				Vector2 originalPosition = AchievmentRect.position;
				Vector2 offScreenPos = new Vector2 (originalPosition.x, originalPosition.y - AchievmentRect.rect.height - 5.0f);
				
				// -- Set it off screen
				AchievmentRect.position = offScreenPos;
				
				// -- Make visible (used to hide for the first time .. );
				CanvasGroup cg = AchievmentRect.GetComponent <CanvasGroup> ();
				cg.alpha = 1.0f;
				
				// -- Set Text
				Text achievementText = AchievmentRect.Find ("_Achievement_Data_Text").GetComponent <Text> ();
				achievementText.text = data.Message + " ( " + data.reward + " )";
				
				// -- Tween this bitch
				StartCoroutine (AchievmentAnimation (AchievmentRect, originalPosition, offScreenPos));
				
				_Bank.Deposit (data.reward);
				
				// -- Save our data .. 
				this.SaveActiveAchievements ();
				this._AchievementManager.SaveAchievements ();
			}
		}
	}

	// UI EVENTS

	private void UpdateGoals ()
	{
		for (int i = 0; i < 3; i++)
		{
			Text _text = GameObject.Find ("Goal_Description_" + (i + 1)).GetComponent <Text> ();
			_text.text = this._ActiveAchievements [i].Message;
		}
	}

	private IEnumerator AchievmentAnimation (RectTransform _ARect, Vector2 to, Vector2 from)
	{
		Tween tweenTo = _ARect.DOMove (to, 0.5f);
		yield return tweenTo.WaitForCompletion ();

		// wait a second
		yield return new WaitForSeconds (1.0f);

		_ARect.DOMove (from, 0.5f);
	}
	
	public void PauseGame ()
	{
		this.paused = true;

		if (GetCurrentPlanet () == null)
			return;

		GetCurrentPlanet ().PauseRotation ();
	}

	public void ResumeGame ()
	{
		StartCoroutine (Resume ());
		GetCurrentPlanet ().ResumeRotation ();
	}

	private IEnumerator Resume ()
	{
		yield return new WaitForSeconds (0.15f);
		this.paused = false;
	}

	void OnDrawGizmos ()
	{
		if (!Application.isPlaying || planetManager == null)
			return;

		planetManager.Draw ();
	}

	public bool IsDead ()
	{
		Vector2 PlayersVPPos = Camera.main.WorldToViewportPoint (this.player.transform.position);

		if (PlayersVPPos.y < -0.4f)
		{
			// start animating camera ... 
			return true;
		}

		return false;
	}

	/// <summary>
	/// Sets the current planet, used for camera animation etc
	/// </summary>
	public void SetCurrentPlanet ()
	{
		this.currentPlanet = planetManager.ClosestPlanet (player.transform.position);
	}
	
	/// <summary>
	/// Tween camera to planet
	/// </summary>
	public void UpdateCamera ()
	{
		if (planetManager.NextPlanet () == null || currentPlanet == null || planetManager.GetTrajectory (currentPlanet) == null)
			return;

		Vector2[] _targets = new Vector2 [3]
		{
			currentPlanet.GetPosition (),
			planetManager.NextPlanet ().GetPosition (),
			planetManager.GetTrajectory (currentPlanet).HighestPoint ()
		};
		
		StartCoroutine (AnimateCamera (0.3f, _targets));

		// shooting star
		if (Random.value > 0.3f)
		{
			Vector2 _position = Camera.main.ViewportToWorldPoint (new Vector2 (Random.Range (0.6f, 1.6f), Random.Range (0.2f, 0.8f)));
			Instantiate (this.visuals.ShootingStar, _position, Quaternion.identity);
		}
	}

	// -- Same as 'Update Camera' but just every frame instead of on planet land ... 
	public void DoCameraEveryFrame ()
	{
		if (currentPlanet == null || planetManager.NextPlanet () == null || planetManager.GetTrajectory (currentPlanet) == null)
			return;

		Vector2[] targets = new Vector2 [3]
		{
			currentPlanet.GetPosition (),
			planetManager.NextPlanet ().GetPosition (),
			planetManager.GetTrajectory (currentPlanet).HighestPoint ()
		};


		Rect boundingBox = CameraExtensions.CalculateTargetsBoundingBox (3.5f, 6.5f, 3.0f, 3.0f, targets);
		
		Vector3 endPosition = CameraExtensions.CalculateCameraPosition (boundingBox, -10.0f); 

		Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, endPosition, Time.deltaTime * 4.0f);

		Camera.main.orthographicSize = CameraExtensions.CalculateOrthographicSize (boundingBox, Camera.main, 5.0f, 5.0f); //Mathf.Lerp (startSize, newSize, t);
	}

	private IEnumerator AnimateCamera (float duration, Vector2[] targets)
	{
		//Rect boundingBox = CameraExtensions.CalculateTargetsBoundingBox (3.5f, 6.5f, 3.0f, 3.0f, targets);
		Rect boundingBox = CameraExtensions.CalculateTargetsBoundingBox (3.5f, 6.5f, 3.0f, 3.0f, targets);

		//Vector3 startPosition = Camera.main.transform.position;
		Vector3 endPosition = CameraExtensions.CalculateCameraPosition (boundingBox, -10.0f); 

		float t = 0.0f;

		Camera.main.transform.DOMove (endPosition, duration).SetEase (Ease.InOutQuad);

		while (t < 1) 
		{
			t += Time.smoothDeltaTime / duration;
			
			// lerp something
			//Camera.main.transform.position = Vector3.Lerp (startPosition, endPosition, t);
			Camera.main.orthographicSize = CameraExtensions.CalculateOrthographicSize (boundingBox, Camera.main, 5.0f, 5.0f); //Mathf.Lerp (startSize, newSize, t);
			
			yield return null;
		}

		yield return null;
	}

	/// <summary>
	/// Saves the active achievements.
	/// </summary>
	void SaveActiveAchievements ()
	{
		PlayerPrefs.SetString ("_ActiveAchievements", SaveUtils.ObjectToStr <List <Achievement>> (this._ActiveAchievements));
	}
	
	/// <summary>
	/// Loads the active achievements.
	/// </summary>
	private List <Achievement> LoadActiveAchievements ()
	{
		return SaveUtils.StrToObject <List <Achievement>> (PlayerPrefs.GetString ("_ActiveAchievements")); 
	}

	public Planet GetCurrentPlanet ()
	{
		return this.currentPlanet;
	}
	
	public Player GetPlayer ()
	{
		return this.player;
	}



    public Texture2D shareTexture;
    

    public void ShareTwitter()
    {
        UM_ShareUtility.TwitterShare("Mars Baby 惑星大ジャンプで" + this.visuals.MetersText.text + " まで進んだ！\n" + storeUrl);
    }

    public void ShareLINE()
    {
        string msg = "Mars Baby 惑星大ジャンプで" + this.visuals.MetersText.text + " まで進んだ！\n" + storeUrl;
        string url = "https://line.me/R/msg/text/?" + WWW.EscapeURL(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(Login, null, null);
            return;
        }
        else
        {
            Login();
        }
    }

    void Login()
    {
        if (!FB.IsLoggedIn)
        {
            FB.Login("", LoginCallback);
            return;
        }
        else
        {
            LoginCallback();
        }
    }

    string FeedLink = "";
    string FeedLinkName = "";
    string FeedLinkDescription = "";
    string FeedPicture = "";
    private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

    void LoginCallback(FBResult result = null)
    {
        if (FB.IsLoggedIn)
        {

            FeedLinkName = "Mars Baby 惑星大ジャンプで ";
            FeedLinkDescription = this.visuals.MetersText.text + " まで進んだ！\n";

            FB.Feed(
                link: storeUrl,
                linkName: FeedLinkName,
                linkDescription: FeedLinkDescription,
                picture: "",
                properties: FeedProperties
            );
        }
    }


    
}