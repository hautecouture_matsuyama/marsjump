﻿using UnityEngine;
using System.Collections;

public class FireBall : MonoBehaviour
{
	private float speed = 1.4f;
	private Vector2 start, end;
	private bool pingPong = false;
	private Vector2 target;


	void Start () 
	{
		Init (4.0f);
	}

	public void Init (float _height)
	{
		Vector2 position = transform.position;

		this.start = new Vector2 (position.x, position.y + _height);
		this.end   = new Vector2 (position.x, position.y - _height);

		Flip ();
	
		Vector2 _target = new Vector2 (target.x - transform.position.x, target.y - transform.position.y);
		float angle = Mathf.Atan2 (_target.y, _target.x) * Mathf.Rad2Deg;
		//transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle + 90.0f));
	}

	public void Flip ()
	{
		pingPong = !pingPong;

		target = pingPong ? start : end;
	}

	void FixedUpdate () 
	{
		Vector2 _target = new Vector2 (target.x - transform.position.x, target.y - transform.position.y);
		
		float angle = Mathf.Atan2 (_target.y, _target.x) * Mathf.Rad2Deg;
		//transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.Euler (new Vector3 (0, 0, angle + 90.0f)), 10.0f * Time.deltaTime);

		transform.position = Vector2.Lerp (transform.position, target, Time.deltaTime * speed);

		if (Vector2.Distance (transform.position, target) < 0.5f)
		{
			Flip ();
		}
	}

	public void KillMe ()
	{
		Destroy (this.gameObject);
	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawLine (start, end);
	
		Gizmos.color = Color.green;
		Gizmos.DrawSphere (target, 0.2f);
	}
}
