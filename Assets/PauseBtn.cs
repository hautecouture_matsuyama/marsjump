﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NendUnityPlugin.AD;

public class PauseBtn : MonoBehaviour {

    [SerializeField]
    private GameObject ActiveObject;

    [SerializeField]
    private GameObject BackPanel;

	[SerializeField]
	private RectTransform Banner;

	[SerializeField]
	private RectTransform Icon;

    [SerializeField]
    private NendAdBanner banner;

    [SerializeField]
    private NendAdBanner pauseRec;


    public void Activation()
    {
        ActiveObject.SetActive(true);

        this.gameObject.SetActive(false);

        ActivePanel();
    }

    void ActivePanel()
    {
        if(BackPanel.activeSelf)
        {
            BackPanel.SetActive(false);
			Banner.localPosition = new Vector2 (-44, 410);
			Icon.localPosition = new Vector2 (200, 410);

            pauseRec.Hide();
            banner.Show();
        }
        else
        {
            BackPanel.SetActive(true);

            pauseRec.Show();
            banner.Hide();

			Banner.localPosition = new Vector2(-44, 240);
			Icon.localPosition = new Vector2(200, 240);
        }
    }


}
