﻿using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour {

    [SerializeField]
    private float speed;

    [SerializeField]
    private float turnPos;

    private Vector2 stratPos;

    void Start()
    {
        stratPos = new Vector2(14, this.transform.position.y);

    }

    void Update()
    {

        if (transform.position.x < turnPos)
        {
 
            transform.position = stratPos;

            speed = Random.RandomRange(0.2f, 0.6f);
        }

        this.transform.Translate(-speed * Time.deltaTime, 0, 0);

        Debug.Log(transform.position.x);

    }
   

}