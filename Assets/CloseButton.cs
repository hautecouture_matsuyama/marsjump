﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NendUnityPlugin.AD;

public class CloseButton : MonoBehaviour {

    public GameObject RankingCanvas;


    public void CloseRanking()
    {
        RankingCanvas.active = false;

        Game.instance.ResumeGame();
    }
}
