﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;

public class NendManager : SingletonMonoBehaviour<NendManager> {

#if UNITY_ANDROID

    [SerializeField]
    private NendAdIcon nendAdIcon;

#endif

    void Awake()
    {
        if (this != Instance)
        {
            Destroy(this);
            return;
        }
    }

    public void NendIconhide()
    {
        //nendAdIcon.Hide();
    }

    public void NendIconShow()
    {
        //nendAdIcon.Show();
    }

}
